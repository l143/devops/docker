# Docker Compose
Docker compose funciona para poder crear varios contenedores como servicios con un solo archivo manifiesto.

Por defecto, todos los servicios que se creen a partir de dicho manifiesto, serán conectados a una red propia de los servicios, por lo que podrán comunicarse directamente por medio de los nombres de servcivio sin hacer uso de las ips.

## Comandos útiles.

> Levantar **servicios** 

```sh
$ docker-compose up [serviceName] -d
```

- **-d** - libera la consola como el detach de docker.

> Ver los **logs** de los servicios 

```sh
$ docker-compose logs [serviceName] 
```

> Ejecutar comando dentro del servicio ó contenedor

```sh
$ docker-compose exec [serviceName] [command]
```

Para solamente construir las imágenes.

```
docker-compose build
```


