use std::{io::Result};
use actix_web::{App, HttpServer, web, middleware::Logger};
use dotenv::dotenv;

use actixapi::{modules};
use tracing::{info};


#[actix_web::main]
async fn main() -> Result<()> {

    dotenv().ok();
    // READ env variables
    let config = modules::config::Config::from_env()
        .expect("Missing env config server");

    // Api base
    let api_base = "/api";

    let server = match HttpServer::new( move || {
        App::new()
            .wrap(Logger::default())
            .service(
                web::scope(&api_base)
                    .configure(modules::api::infra::endpoints::config)
            )
    })
    .bind(format!("{}:{}", config.server.host, config.server.port)) {
        Ok(server) => server,
        Err(e) => panic!("{}", e)
    };

    info!("🚪 api-> {}", &api_base);
    server.run().await
}