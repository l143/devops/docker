use actix_web::{Responder, HttpResponse, web};


async fn hello() -> impl Responder {
    let body = " {\"hello\": \"world\"} ";
    HttpResponse::Ok()
        .content_type("application/json")
        .body(body)
}

async fn echo(path: web::Path<(String,)>,) -> impl Responder {
    let info = path.into_inner();
    let body =  format!("echo: {} ", info.0); 
    HttpResponse::Ok()
        .content_type("text/plain")
        .body(body)
 }


pub fn config(cfg: &mut web::ServiceConfig) {
    cfg.service(
        web::resource("/")
            .route(web::get().to(hello))
    )
    .service(
        web::resource("/echo/{id}")
            .route(web::get().to(echo))
    );
}