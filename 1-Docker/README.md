# Docker 
Docker es una tecnología basada en el concepto de virtualización para poder replicar aplicaciones sin tener que preocuparnos por en entorno dónde vivirán.

## 🛠 **Comandos útiles**

---

> Listar los **contenedores**
```sh
docker ps -a
```
 - **-a** - Mostrará también los contenedores detenidos

> Listas las **imágenes**
```sh
docker images
```

> Obtener una **imagen** de dockerhub
```sh
docker pull [imageName]
```

> Crear un contenedor a partir de una imagen.
```
docker run --name [containerName] --detach imageName 
```
- **--name** Le especifica un nombre al contenedor que creará
- **--detach** En caso de que el comando principal del contendor se deba quedar escuchando, liberara la terminal del host

> Ejecutar un *comando* en un **contenedor**
```sh
docker exec [conatinerID | containerName] [command]
```

Si queremos ligar el comando a nuestra temrinal como en el caso de un a sesión bash, debemos agregar la bandera **-it**.

```sh
docker exec -it [conatinerID | containerName] [command] bash
```

> Ligar un puerto al contendor   
(*Esto deberá realizarse a la hora de crear el contenedor*)
```sh
docker run --name srv -d -p 9090:80 nginx:1.20.1
```
- **-p** Asigna un puerto del *HOST* al *CONTAINER*

---
## 💾 Volúmenes  
<br>

> Ligar un path al **contendor** (bind mount)  
(*Esto deberá realizarse a la hora de crear el contenedor*)

```sh
docker run \
    --name srv \
    -v "$(pwd)/html":/usr/share/nginx/html:ro \
    -p 9090:80 \
    -d nginx:1.20.1
```
- **-v** Hace un bind de un path en *HOST* al *CONTAINER*

> Listar los **volúmenes**
```sh
docker volumne ls
```

> Crear un **volumen**
```
docker volume create htmlnginx
```

> Crear un contenedor con un **volúmen**
```
docker run \
    --name srv \
    --mount src=htmlnginx,dst=/usr/share/nginx/html \
    -p 9090:80 \
    -d nginx:1.20.1
```

> Enviar un archivo al contenedor
```
docker cp [localSrcFilePath] [contanerName]:[containerDstPath]
```

> Extraer un archivo del contenedor
```
docker cp [contanerName]:[containerDstPath] [localSrcFilePath] 
```

---
## 📷 Imágenes  
<br>

> Crear una imágen
```
docker build -t [imageTag] -f [dockerFile] [context] 
```

> Historial de una imagen
```
docker history [imageName]
```

> Inspeccionar una imagen
```
dive [imageTag]
```

---
## 🕸 Redes  
<br>

> Listar las **redes**
```
docker network ls
```

> Crear una **red**
```
docker network create --attachable [networkName]
```
 - **--atachable** Indica que se pueden agregar contenedores a la red.

> Conectar un contenedor a una **red**

```
docker network connect [networkName] [containerName]
```


---
## 🧹 Limpiar  
<br>


> Eliminar **contenedores** que no se estén usando.
```
docker container prune
```

> Eliminar **redes** que no se estén usando.

```
docker network prune
```


> Eliminar **volúmens** que no se estén usando.

```
docker volume prune
```

> Eliminar **recursos** que no se estén usando.

```
docker system prune

```

## Referencias
- https://docs.docker.com/engine/install/ubuntu/
- https://docs.docker.com/config/containers/resource_constraints/