#!/bin/bash

# Obtenemos el ejecutable de docker-compose

curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose

# Cambiamos permisos de ejecución

chmod +x /usr/local/bin/docker-compose