# Instalaciòn de docker y docker compose

Los scripts listados ayudarán a instalar docker y docker compose respectivamente en un sistema linux basado en debian.

Por defecto se instalará la última versión estable para ubuntu focal.


Para poder usar el comando de docker después de hacer una instalación, debe añadir el usuario que usara el comando al grupo docker.

> Creamos el comando
```
$ sudo groupadd docker
```

> Agregamos el usuario al grupo
```
$ sudo usermod -aG docker [osUser]
```


Posteriormente se debe reiniciar el shell o bien ejecutar la siguiente instrucción.

```
$ newgrp docker
```