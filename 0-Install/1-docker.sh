#!/bin/bash

# Actualizamos repos
apt-get update;

# Instalamos dependencias de docker
apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg \
    lsb-release

# Agregamos la llave gpg
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg

# Agregamos a apt sources list el repo de focal ubuntu
echo \
  "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
  focal stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null


# Actualizamos repos de nuevo
apt-get update;

# Instalamos docker
apt-get install docker-ce docker-ce-cli containerd.io;